from utils.print_color import OKGREEN, ENDC
from utils.plot_util import *
from algorithms.make_change_greedy_solution import make_change_greedy_solution_store_in_file, make_change_greedy_solution_print
from algorithms.make_change_all_solutions_recursive import make_change_all_solutions_recursive_store_in_file, make_change_all_solutions_recursive_print
from algorithms.make_change_all_solutions_iterative import make_change_all_solutions_iterative_store_in_file, make_change_all_solutions_iterative_print
from algorithms.make_change_solutions_recursive_with_cut import make_change_solutions_recursive_with_cut_store_in_file, make_change_solutions_recursive_with_cut_print
from algorithms.make_change_best_solutions_recursive import make_change_best_solutions_recursive_store_in_file, make_change_best_solutions_recursive_print
from tests.test_make_change_algorithms import run_computation_time_test_store_in_file, run_computation_time_test_print

class MakeChangeFileAlgo:
    def __init__(self, name, func, file_name) -> None:
        self.name = name
        self.func = func
        self.file_name = file_name

class MakeChangePrintAlgo:
    def __init__(self, name, func) -> None:
        self.name = name
        self.func = func

class MakeChangeFilePrintAlgo:
    def __init__(self, name, func_file, func_print, file_name) -> None:
        self.name = name
        self.func_file = func_file
        self.func_print = func_print
        self.file_name = file_name

def get_labels(algorithms):
    labels = []

    for algorithm in algorithms: 
        labels.append(algorithm.name)

    return labels
        
def test_make_change_store_in_file_comparison():
    print(f'{OKGREEN}RUN TEST: Make change algorithms, comparison time for storing in a file.{ENDC}')

    greedy_algo = MakeChangeFileAlgo('greedy solution', make_change_greedy_solution_store_in_file, 'make_change_greedy_solution.csv')
    all_solutions_recursive_algo = MakeChangeFileAlgo('all solutions using recursive approach', make_change_all_solutions_recursive_store_in_file, 'make_change_all_solutions_recursive.csv')
    all_solutions_iterative_algo = MakeChangeFileAlgo('all solutions using iterative approach', make_change_all_solutions_iterative_store_in_file, 'make_change_all_solutions_iterative.csv')
    solutions_recursive_with_cut_algo = MakeChangeFileAlgo('solutions with cut using recursive', make_change_solutions_recursive_with_cut_store_in_file, 'make_change_solutions_with_cut_recursive.csv')
    best_solutions_recursive_algo = MakeChangeFileAlgo('best solution(s) using recursive approach', make_change_best_solutions_recursive_store_in_file, 'make_change_best_solutions_recursive.csv')

    make_change_algorithms = (greedy_algo, all_solutions_recursive_algo, all_solutions_iterative_algo, solutions_recursive_with_cut_algo, best_solutions_recursive_algo)
    elapsed_times = []

    for algo in make_change_algorithms:
        elapsed_times.append(run_computation_time_test_store_in_file(algo.file_name, algo.func, 6))

    options = {
        'xlabel': 'algorithms',
        'ylabel': 'Execution time (s)',
        'title': 'Execution time comparison for storing in a file'
    }

    plot_bar_histogram(elapsed_times, get_labels(make_change_algorithms), options)

    print(f'{OKGREEN}TEST FINISHED{ENDC}\n')

def test_make_change_print_comparison():
    print(f'{OKGREEN}RUN TEST: Make change algorithms, comparison time for printing into the terminal.{ENDC}')

    greedy_algo = MakeChangePrintAlgo('greedy solution', make_change_greedy_solution_print)
    all_solutions_recursive_algo = MakeChangePrintAlgo('all solutions using recursive approach', make_change_all_solutions_recursive_print)
    all_solutions_iterative_algo = MakeChangePrintAlgo('all solutions using iterative approach', make_change_all_solutions_iterative_print)
    solutions_recursive_with_cut_algo = MakeChangePrintAlgo('solutions with cut using recursive', make_change_solutions_recursive_with_cut_print)
    best_solutions_recursive_algo = MakeChangePrintAlgo('best solution(s) using recursive approach', make_change_best_solutions_recursive_print)

    make_change_algorithms = (greedy_algo, all_solutions_recursive_algo, all_solutions_iterative_algo, solutions_recursive_with_cut_algo, best_solutions_recursive_algo)
    elapsed_times = []

    for algo in make_change_algorithms:
        elapsed_times.append(run_computation_time_test_print(algo.func, 6))

    options = {
        'xlabel': 'algorithms',
        'ylabel': 'Execution time (s)',
        'title': 'Execution time comparison for printing'
    }

    plot_bar_histogram(elapsed_times, get_labels(make_change_algorithms), options)

    print(f'{OKGREEN}TEST FINISHED{ENDC}\n')

def test_make_change_print_and_store_in_file_comparison():
    print(f'{OKGREEN}RUN TEST: Make change algorithms, comparison time for storing in a file and printing into the terminal.{ENDC}\n')

    greedy_algo = MakeChangeFilePrintAlgo('greedy solution', make_change_greedy_solution_store_in_file, make_change_greedy_solution_print, 'make_change_greedy_solution.csv')
    all_solutions_recursive_algo = MakeChangeFilePrintAlgo('all solutions using recursive approach', make_change_all_solutions_recursive_store_in_file, make_change_all_solutions_recursive_print, 'make_change_all_solutions_recursive.csv')
    all_solutions_iterative_algo = MakeChangeFilePrintAlgo('all solutions using iterative approach', make_change_all_solutions_iterative_store_in_file, make_change_all_solutions_iterative_print, 'make_change_all_solutions_iterative.csv')
    solutions_recursive_with_cut_algo = MakeChangeFilePrintAlgo('solutions with cut using recursive', make_change_solutions_recursive_with_cut_store_in_file, make_change_solutions_recursive_with_cut_print, 'make_change_solutions_with_cut_recursive.csv')
    best_solutions_recursive_algo = MakeChangeFilePrintAlgo('best solution(s) using recursive approach', make_change_best_solutions_recursive_store_in_file, make_change_best_solutions_recursive_print, 'make_change_best_solutions_recursive.csv')

    make_change_algorithms = (greedy_algo, all_solutions_recursive_algo, all_solutions_iterative_algo, solutions_recursive_with_cut_algo, best_solutions_recursive_algo)
    elapsed_times_store_in_file = []
    elapsed_times_print = []

    for algo in make_change_algorithms:
        elapsed_times_store_in_file.append(run_computation_time_test_store_in_file(algo.file_name, algo.func_file, 6))
        elapsed_times_print.append(run_computation_time_test_print(algo.func_print, 6))

    options = {
        'xlabel': 'algorithms',
        'ylabel': 'Execution time (s)',    
        'data1_label': 'store in file',
        'data2_label': 'print',
        'title': 'Execution time comparison for printing and storing in a file'
    }

    plot_grouped_bar_histogram(elapsed_times_store_in_file, elapsed_times_print, get_labels(make_change_algorithms), options)

    print(f'\n{OKGREEN}TEST FINISHED{ENDC}\n')
