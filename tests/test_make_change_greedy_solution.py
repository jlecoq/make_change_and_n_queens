from algorithms.make_change_greedy_solution import make_change_greedy_solution_print, make_change_greedy_solution_store_in_file
from tests.test_make_change_algorithms import *

def test_greedy_solution_store_in_file():
    start_message = "Compute a greedy solution of the make change problem and store it in a file:"
    file_name = "make_change_greedy_solution.csv"
    final_message = f'The greedy solution have been stored in the file: "{file_name}"\n'

    run_test_store_in_file(start_message, final_message, file_name, make_change_greedy_solution_store_in_file)

def test_greedy_solution_print():
    start_message = 'Compute a greedy solution of the make change problem and print it:'
    run_test_print(start_message, make_change_greedy_solution_print)