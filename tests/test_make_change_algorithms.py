from utils.print_color import OKGREEN, ENDC
from utils.math_util import round_half_up
from time import perf_counter

def run_computation_time_test_store_in_file(file_name, test_function, precision = 3):
    coins = (5, 2, 1, 0.5, 0.2, 0.1, 0.05)
    money_to_return = 12.35
    file_name = f'csv_results/{file_name}'

    initial_timestamp = perf_counter()
    test_function(
        coins, money_to_return, file_name)
    final_timestamp = perf_counter()

    return round_half_up(final_timestamp - initial_timestamp, precision)
    
def run_computation_time_test_print(test_function, precision = 3):
    coins = (5, 2, 1, 0.5, 0.2, 0.1, 0.05)
    money_to_return = 12.35

    initial_timestamp = perf_counter()
    test_function(
        coins, money_to_return)
    final_timestamp = perf_counter()

    return round_half_up(final_timestamp - initial_timestamp, precision)
    
def run_test_store_in_file(test_message, final_message, file_name, test_function):
    print(f'{OKGREEN}RUN TEST: {test_message}\n{ENDC}')

    coins = (5, 2, 1, 0.5, 0.2, 0.1, 0.05)
    money_to_return = 12.35
    file_name = f'csv_results/{file_name}'

    print(f'The coins: {coins}')
    print(f'The money to return {money_to_return}\n')

    # Perform the algorithm and display the computation time.
    initial_timestamp = perf_counter()
    test_function(
        coins, money_to_return, file_name)
    final_timestamp = perf_counter()
    elapsed_time = final_timestamp - initial_timestamp

    # print(f'The best solution(s) have been stored in the file: "{file_name}"\n')
    print(final_message)
    print(f'Execution time: {elapsed_time:0.7f} seconds.\n')

    print(f'{OKGREEN}TEST FINISHED{ENDC}\n')


def run_test_print(test_message, test_function):
    print(f'{OKGREEN}RUN TEST: {test_message}\n{ENDC}')

    coins = (5, 2, 1, 0.5, 0.2, 0.1, 0.05)
    money_to_return = 12.35

    print(f'The coins: {coins}')
    print(f'The money to return {money_to_return}\n')

    # Perform the algorithm and display the computation time.
    initial_timestamp = perf_counter()
    test_function(coins, money_to_return)
    final_timestamp = perf_counter()
    elapsed_time = final_timestamp - initial_timestamp

    print()
    print(f'Execution time: {elapsed_time:0.7f} seconds.\n')

    print(f'{OKGREEN}TEST FINISHED{ENDC}\n')
