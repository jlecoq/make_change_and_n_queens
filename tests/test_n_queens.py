from utils.print_color import OKGREEN, ENDC
from algorithms.n_queens import solve_NQ

def test_n_queens_board_of_size_4():
    print(f'{OKGREEN}RUN TEST: execute n queens algorithm with a board of 4 * 4\n{ENDC}')

    solve_NQ(4)

    print(f'\n{OKGREEN}TEST FINISHED{ENDC}\n')