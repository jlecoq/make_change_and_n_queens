from algorithms.make_change_solutions_recursive_with_cut import make_change_solutions_recursive_with_cut_store_in_file, make_change_solutions_recursive_with_cut_print
from tests.test_make_change_algorithms import *

def test_make_change_solutions_recursive_with_cut_store_in_file():
    start_message = "Compute solutions of the make change problem using a recursive approach that improve the previous one already computed and store them in a file:"
    file_name = 'make_change_solutions_with_cut_recursive.csv'
    final_message = f'All the solutions have been stored in the file: "{file_name}"\n'

    run_test_store_in_file(start_message, final_message, file_name, make_change_solutions_recursive_with_cut_store_in_file)

def test_make_change_solutions_recursive_with_cut_print():
    start_message = 'Compute solutions of the make change problem using a recursive approach that improve the previous one already computed and print them:'
    run_test_print(start_message, make_change_solutions_recursive_with_cut_print)