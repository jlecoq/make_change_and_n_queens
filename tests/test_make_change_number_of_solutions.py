from algorithms.count_all_solutions_make_change import count
from utils.print_color import OKGREEN, ENDC
from glob import glob
import platform

def test_make_change_number_of_solutions_print():
    print(f'{OKGREEN}RUN TEST: Count the total number of solutions\n{ENDC}')

    coins = (5, 2, 1, 0.5, 0.2, 0.1, 0.05)
    money_to_return = 12.35

    print(f'The coins: {coins}')
    print(f'The money to return {money_to_return}\n')

    print(f'The number of solutions: {count(coins, money_to_return)}\n')

    print(f'{OKGREEN}TEST FINISHED{ENDC}\n')

def test_make_change_count_line_files():
    print(f'{OKGREEN}RUN TEST: Count the number of solutions given by each algorithm (stored in files)\n{ENDC}')

    for file_path in glob("./csv_results/*.csv"):    
        if platform.system() == 'Linux':
            file_name = file_path.split('/')[1]
        else:
            file_name = file_path.split('\\')[1]

        count = 0
        
        for _ in open(file_path):
            count += 1

        print(f'The file "{file_name}" contains: {count} lines.')
    
    print(f'\n{OKGREEN}TEST FINISHED{ENDC}\n')
