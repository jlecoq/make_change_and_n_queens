from utils.print_color import OKGREEN, ENDC
from time import perf_counter
from algorithms.make_change_best_solutions_recursive import make_change_best_solutions_recursive_print, make_change_best_solutions_recursive_store_in_file
from tests.test_make_change_algorithms import run_test_store_in_file, run_test_print

def test_make_change_best_solutions_recursive_store_in_file():
    start_message = "Compute the best solution(s) of the make change problem using a recursive approach and store it/them in a file:"
    file_name = 'make_change_best_solutions_recursive.csv'
    final_message = f'The best solution(s) have been stored in the file: "{file_name}"\n'

    run_test_store_in_file(start_message, final_message, file_name, make_change_best_solutions_recursive_store_in_file)

def test_make_change_best_solutions_recursive_print():
    start_message = 'Compute the best solution(s) of the make change problem using a recursive approach and print it/them:'
    run_test_print(start_message, make_change_best_solutions_recursive_print)
