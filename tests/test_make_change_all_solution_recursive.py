from time import perf_counter
from algorithms.make_change_all_solutions_recursive import make_change_all_solutions_recursive_store_in_file, make_change_all_solutions_recursive_print
from utils.print_color import OKGREEN, ENDC
from tests.test_make_change_algorithms import *

def test_make_change_all_solutions_recursive_store_in_file():
    start_message = "Compute all solutions of the make change problem using a recursive approach and store them in a file:"
    file_name = 'make_change_all_solutions_recursive.csv'
    final_message = f'All the solutions have been stored in the file: "{file_name}"\n'

    run_test_store_in_file(start_message, final_message, file_name, make_change_all_solutions_recursive_store_in_file)

def test_make_change_all_solutions_recursive_print():
    start_message = 'Compute all solutions of the make change problem using a recursive approach and print them:'
    run_test_print(start_message, make_change_all_solutions_recursive_print)