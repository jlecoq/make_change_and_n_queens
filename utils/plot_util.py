import matplotlib.pyplot as plt


def autolabel(rects, ax):
    """Attach a text label above each bar in *rects*, displaying its height."""
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')


def plot_bar_histogram(data: list, labels: list, options: dict):
    # Set width of bar
    bar_width = 0.25

    # Set position of bar on X axis
    r1 = [x for x in range(len(labels))]

    fig, ax = plt.subplots()

    # Make the plot
    rects1 = ax.bar(r1, data, bar_width)

    # Add xticks on the middle of the group bars
    ax.ticklabel_format(useOffset=False, style='plain')
    ax.set_xticks(r1)
    ax.set_xticklabels(labels)

    autolabel(rects1, ax)

    fig.tight_layout()

    # Create legend & Show graphic
    if 'ylabel' in options:
        plt.ylabel(options['ylabel'], fontweight='bold')

    if 'xlabel' in options:
        plt.xlabel(options['xlabel'], fontweight='bold')

    if 'title' in options:
        plt.title(options['title'], fontweight='bold')

    plt.show()


def plot_grouped_bar_histogram(data1: list, data2: list, labels: list, options: dict):
    # Set width of bar
    bar_width = 0.25

    # Set position of bar on X axis
    r1 = [x for x in range(len(labels))]
    r2 = [x - bar_width / 2 for x in r1]
    r3 = [x + bar_width / 2 for x in r1]

    fig, ax = plt.subplots()

    # Make the plot
    rects1 = ax.bar(r2, data1, bar_width, label=options['data1_label'])
    rects2 = ax.bar(r3, data2, bar_width, label=options['data2_label'])

    # Add xticks on the middle of the group bars
    ax.ticklabel_format(useOffset=False, style='plain')
    ax.set_xticks(r1)
    ax.set_xticklabels(labels)

    autolabel(rects1, ax)
    autolabel(rects2, ax)

    fig.tight_layout()

    # Create legend & Show graphic
    plt.legend()

    if 'ylabel' in options:
        plt.ylabel(options['ylabel'], fontweight='bold')

    if 'xlabel' in options:
        plt.xlabel(options['xlabel'], fontweight='bold')

    if 'title' in options:
        plt.title(options['title'], fontweight='bold')

    plt.show()
