from math import floor

def round_half_up(n, decimals=0):
    multiplier = 10 ** decimals
    return floor(n * multiplier + 0.5) / multiplier