def number_of_decimals(number):
    """
        Get the number of decimals of a number.
    """

    number_parts = str(number).split(".")  # Get the number of decimals.

    if (len(number_parts) == 2):
        return len(number_parts[1])

    return 0

def to_integers(coins, money_to_return):
    """
        Transform the coins and the money to return into integer values.
    """

    max_number_of_decimals = 0

    # Update the max number of decimals if a coin has
    # more decimals than the current maximum number of decimals.
    for coin in coins:
        num_of_decimals = number_of_decimals(coin)

        if num_of_decimals > max_number_of_decimals:
            max_number_of_decimals = num_of_decimals

    # Update the max number of decimals if the money to return has
    # more decimals than the current maximum number of decimals.
    num_of_decimals = number_of_decimals(money_to_return)

    if num_of_decimals > max_number_of_decimals:
        max_number_of_decimals = num_of_decimals

    # The factor of conversion.
    factor = 10**max_number_of_decimals

    # Convert to integer the coins and money to return.
    coins = [int(coin * factor) for coin in coins]
    money_to_return = int(money_to_return * factor)

    return coins, money_to_return, factor

def print_solution(coins_counter):
    print(f'Solution: {coins_counter}')