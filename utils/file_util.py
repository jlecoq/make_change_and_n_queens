def write_headers_to_csv(file, coins):
    """
        Write the coins header to a csv file.
    """

    for coin in coins:
        file.write(f'Number of coins {coin} required;')

    file.write('\n')

def write_coins_to_file(file):
    def write_coins(coins_counter):
        for element in coins_counter:
            file.write(f'{element};')

        file.write('\n')
    
    return write_coins

    