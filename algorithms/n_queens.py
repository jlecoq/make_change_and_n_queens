''' Python3 program to solve N Queen Problem using 
backtracking '''
k = 1


def create_board(size):
    return [[0 for j in range(size)]
            for i in range(size)]


# A utility function to print solution.
def print_solution(board):
    global k

    print('=====================')
    print(f'Queens placement - {k}')
    print('=====================\n')

    k += 1
    board_size = len(board)

    for i in range(board_size):
        for j in range(board_size):
            print(board[i][j], end=" ")
        print("\n")


def print_board(board):
    board_size = len(board)
    print('=====================')
    print(f'The board ({board_size} * {board_size})')
    print('=====================\n')

    for i in range(board_size):
        for j in range(board_size):
            print(board[i][j], end=" ")
        print("\n")


''' A utility function to check if a queen can 
be placed on board[row][col]. Note that this 
function is called when "col" queens are 
already placed in columns from 0 to col -1. 
So we need to check only left side for 
attacking queens '''


def is_safe(board, row, col):
    board_size = len(board)

    # Check this row on left side
    for i in range(col):
        if (board[row][i]):
            return False

    # Check upper diagonal on left side
    i = row
    j = col
    while i >= 0 and j >= 0:
        if(board[i][j]):
            return False
        i -= 1
        j -= 1

    # Check lower diagonal on left side
    i = row
    j = col
    while j >= 0 and i < board_size:
        if(board[i][j]):
            return False
        i = i + 1
        j = j - 1

    return True


''' A recursive utility function to solve N 
Queen problem '''


def solve_NQ_util(board, col):
    board_size = len(board)
    ''' base case: If all queens are placed 
    then return true '''
    if (col == board_size):
        print_solution(board)
        return True

    ''' Consider this column and try placing 
	this queen in all rows one by one '''
    res = False
    for i in range(board_size):

        ''' Check if queen can be placed on 
        board[i][col] '''
        if (is_safe(board, i, col)):

            # Place this queen in board[i][col]
            board[i][col] = 1

            # Make result true if any placement
            # is possible
            res = solve_NQ_util(board, col + 1) or res

            ''' If placing queen in board[i][col] 
			doesn't lead to a solution, then 
			remove queen from board[i][col] '''
            board[i][col] = 0  # BACKTRACK

    ''' If queen can not be place in any row in 
		this column col then return false '''
    return res


''' This function solves the N Queen problem using 
Backtracking. It mainly uses solveNQUtil() to 
solve the problem. It returns false if queens 
cannot be placed, otherwise return true and 
prints placement of queens in the form of 1s. 
Please note that there may be more than one 
solutions, this function prints one of the 
feasible solutions.'''


def solve_NQ(board_size):
    board = create_board(board_size)
    print_board(board)

    if solve_NQ_util(board, 0) == False:
        print("Solution does not exist")

