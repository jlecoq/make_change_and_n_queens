from utils.various_util import *

# Dynamic Programming Python implementation of Coin  
# Change problem 
def count(coins, money_to_return):     
    coins, money_to_return, _ = to_integers(coins, money_to_return)
    m = len(coins)
    # table[i] will be storing the number of solutions for 
    # value i. We need n+1 rows as the table is constructed 
    # in bottom up manner using the base case (n = 0) 
    # Initialize all table values as 0 
    table = [0 for k in range(money_to_return + 1)] 
  
    # Base case (If given value is 0) 
    table[0] = 1
  
    # Pick all coins one by one and update the table[] values 
    # after the index greater than or equal to the value of the 
    # picked coin 
    for i in range(0,m): 
        for j in range(coins[i], money_to_return + 1): 
            table[j] += table[j - coins[i]] 
  
    return table[money_to_return] 

# def main():
    # coins = [5, 2, 1, 0.5, 0.2, 0.1, 0.05]
    # money_to_return = 12.35
# 
    # coins, money_to_return = to_integers(coins, money_to_return)
# 
    # print(count(coins, money_to_return))
# 
# main()