from utils.file_util import *
from utils.various_util import *

def make_change_all_solutions_recursive_(coins, money_to_return, coins_counter, index, action_cb):
    """
        Compute all the solutions of the make change problem recursively.
        For each solution computed, execute the callback "action_cb".
    """

    if money_to_return == 0:
        # A solution has been computed, execute the action.
        action_cb(coins_counter)
        return

    if index == len(coins):
        return

    currCoin = coins[index]

    i = 0
    while i * currCoin <= money_to_return:
        coins_counter[index] = i
        make_change_all_solutions_recursive_(
            coins, money_to_return - i * currCoin, coins_counter, index + 1, action_cb)
        coins_counter[index] = 0
        i += 1

def make_change_all_solutions_recursive_store_in_file(coins, money_to_return, file_name):
    """
        Execute the make change all solutions recursively algorithm.
        Every solution are stored in a csv file.
    """

    file = open(file_name, "w")

    # Write the header of the csv file.
    write_headers_to_csv(file, coins)

    # Transform my coins and the money to return into integer variables.
    coins, money_to_return, _ = to_integers(coins, money_to_return)

    coins_counter = [0] * len(coins)

    write_coins = write_coins_to_file(file)

    make_change_all_solutions_recursive_(coins, money_to_return, coins_counter, 0, write_coins)

def make_change_all_solutions_recursive_print(coins, money_to_return):
    """
        Execute the make change all solutions recursively algorithm.
        Every solution are print in the console.
    """

    # Transform my coins and the money to return into integer variables.
    coins, money_to_return, _ = to_integers(coins, money_to_return)

    coins_counter = [0] * len(coins)

    make_change_all_solutions_recursive_(coins, money_to_return, coins_counter, 0, print_solution)
