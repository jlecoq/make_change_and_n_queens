from utils.file_util import *
from utils.various_util import *

def make_change_greedy_solution_(coins, money_to_return):
    coins_needed = [0] * len(coins)

    # Loop through every coins.
    for index, coin in enumerate(coins):
        #  Compute the number of a specific coin required.
        number_of_coins = int(money_to_return / coin)
        # Update the amout of money to return.
        money_to_return = money_to_return % coin

        # Print the number of a specific coin required.
        if number_of_coins != 0:
            coins_needed[index] = number_of_coins

    return coins_needed

def print_greedy_solution(coins, coins_counter):
    for i in range(len(coins)):
        print(f'{coins_counter[i]} coins of {coins[i]}.')

def make_change_greedy_solution_print(coins, money_to_return):
    print('The greedy solution is:')

    greedy_solution = make_change_greedy_solution_(coins, money_to_return)
    print_greedy_solution(coins, greedy_solution)

def make_change_greedy_solution_store_in_file(coins, money_to_return, file_name):
    file = open(file_name, "w")

    # Write the header of the csv file.
    write_headers_to_csv(file, coins)
    
    greedy_solution = make_change_greedy_solution_(coins, money_to_return)

    write_coins_to_file(file)(greedy_solution)
