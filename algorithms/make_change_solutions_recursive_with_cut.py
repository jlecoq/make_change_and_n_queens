from utils.file_util import *
from utils.various_util import *
from sys import maxsize

best_solution = []
stop_recursive_call = False
min_number_of_coins = maxsize

def make_change_solutions_recursive_with_cut_(coins, money_to_return, coins_counter, index, action_cb):
    """
        Store in a csv file the solutions of the make change problem that improve the current best solution computed.
    """

    if money_to_return == 0:
        # Store the solution in the file.
        number_of_coins = sum(coins_counter)

        global min_number_of_coins
        global best_solution
        global stop_recursive_call

        # I check if the current solution improve the previous one.
        if number_of_coins < min_number_of_coins: 
            min_number_of_coins = number_of_coins
            best_solution = coins_counter.copy()            
        # If not, I stop the recursive calls.
        else:                            
            stop_recursive_call = True
            return

        action_cb(coins_counter)
        return

    if index == len(coins):
        return

    currCoin = coins[index]

    i = 0
    while i * currCoin <= money_to_return:
        coins_counter[index] = i

        if stop_recursive_call is not True:
            make_change_solutions_recursive_with_cut_(coins, money_to_return - i * currCoin, coins_counter, index + 1, action_cb)
            coins_counter[index] = 0
            i += 1
        else:
            return

def print_improved_solution(coins_counter):
    print(f'This solution is better than the previous one: {coins_counter}')

def make_change_solutions_recursive_with_cut_print(coins, money_to_return):
    # Transform my coins and the money to return into integer variables.
    coins, money_to_return, _ = to_integers(coins, money_to_return)

    coins_counter = [0] * len(coins)

    make_change_solutions_recursive_with_cut_(coins, money_to_return, coins_counter, 0, print_improved_solution)

def make_change_solutions_recursive_with_cut_store_in_file(coins, money_to_return, file_name):
    file = open(file_name, "w")

    # Write the header of the csv file.
    write_headers_to_csv(file, coins)

    # Transform my coins and the money to return into integer variables.
    coins, money_to_return, _ = to_integers(coins, money_to_return)

    coins_counter = [0] * len(coins)

    write_coins = write_coins_to_file(file)

    make_change_solutions_recursive_with_cut_(coins, money_to_return, coins_counter, 0, write_coins)