from sys import maxsize
from utils.file_util import *
from utils.various_util import *

best_solution = []
probably_best_solutions = []
min_number_of_coins = maxsize

def make_change_best_solutions_(coins, money_to_return, coins_counter, index):
    if money_to_return == 0:        
        number_of_coins = sum(coins_counter)

        global best_solution
        global probably_best_solutions
        global min_number_of_coins

        if number_of_coins < min_number_of_coins:
            min_number_of_coins = number_of_coins
            best_solution = coins_counter.copy()           
            probably_best_solutions.append(best_solution)
        elif number_of_coins == min_number_of_coins:
            probably_best_solutions.append(coins_counter.copy())
                                        
        return

    if index == len(coins):
        return

    currCoin = coins[index]

    i = 0
    while i * currCoin <= money_to_return:
        coins_counter[index] = i
        make_change_best_solutions_(
            coins, money_to_return - i * currCoin, coins_counter, index + 1)
        coins_counter[index] = 0
        i += 1

def reset_data():
    global best_solution
    global probably_best_solutions
    global min_number_of_coins

    best_solution = []
    probably_best_solutions = []
    min_number_of_coins = maxsize

def to_float(coins, factor):
    return [x / float(factor) for x in coins]

def get_best_solutions(probably_best_solutions, best_solution):
    best_solutions = []
    best_solution_sum = sum(best_solution)

    for probably_best_solution in probably_best_solutions:
        if sum(probably_best_solution) == best_solution_sum:        
            best_solutions.append(probably_best_solution)

    return best_solutions

def print_best_solution(coins, coins_counter):
    for i in range(len(coins)):
        print(f'{coins_counter[i]} coins of {coins[i]}.')

def make_change_best_solutions_recursive_print(coins, money_to_return):
    # Transform my coins and the money to return into integer variables.
    coins, money_to_return, factor = to_integers(coins, money_to_return)    

    coins_counter = [0] * len(coins)

    make_change_best_solutions_(coins, money_to_return, coins_counter, 0)    
    
    coins = to_float(coins, factor)

    global best_solution
    best_solutions = get_best_solutions(probably_best_solutions, best_solution)

    if len(best_solutions) == 1:
        print('The best solution is:')
        print_best_solution(coins, best_solutions[0])
    else:
        print('We have several best solutions.')
        print('List of the best solutions:')
        
        for best_sol in best_solutions:
            print_best_solution(coins, best_sol)

    reset_data()

def make_change_best_solutions_recursive_store_in_file(coins, money_to_return, file_name):
    file = open(file_name, "w")

    # Write the header of the csv file.
    write_headers_to_csv(file, coins)

    # Transform my coins and the money to return into integer variables.
    coins, money_to_return, _ = to_integers(coins, money_to_return)    

    coins_counter = [0] * len(coins)

    make_change_best_solutions_(coins, money_to_return, coins_counter, 0)    

    global best_solution
    best_solutions = get_best_solutions(probably_best_solutions, best_solution)

    write_coins = write_coins_to_file(file)

    for best_solution in best_solutions:
        write_coins(best_solution)
    
    reset_data()