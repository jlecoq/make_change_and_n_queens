from utils.file_util import *
from utils.various_util import *

def make_change_all_solutions_iterative_(coins, money_to_return, coins_to_index, action_cb):
    """
        Compute all the solutions of the make change problem iteratively.
        For each solution computed, execute the callback "action_cb".
    """

    wallets = [[coin] for coin in coins]
    new_wallets = []

    while wallets:
        for wallet in wallets:
            s = sum(wallet)
            for coin in coins:
                if coin >= wallet[-1]:
                    if s + coin < money_to_return:
                        new_wallets.append(wallet + [coin])
                    elif s + coin == money_to_return:
                        # Transform my wallet into a list of counters.
                        coins_counter = [0] * len(coins)
                        to_counter(wallet + [coin],
                                   coins_counter, coins_to_index)

                        action_cb(coins_counter)
        wallets = new_wallets
        new_wallets = []


def to_counter(coins_list, coins_counter, coins_to_index):
    """
        Transform a list of coins into a list of coins counter.
    """

    for coin in coins_list:
        index = coins_to_index[coin]
        coins_counter[index] += 1


def coins_matching(coins):
    """
        Return a dictionnary that make match the coin 
        with its index where it should be stored.
    """

    coinsToIndex = {}

    for i in range(len(coins)):
        coinsToIndex[coins[i]] = i

    return coinsToIndex

def make_change_all_solutions_iterative_store_in_file(coins, money_to_return, file_name):
    """
        Execute the make change all solutions iteratively algorithm.
        Every solution are stored in a csv file.
    """

    file = open(file_name, "w")

    # Write the header of the csv file.
    write_headers_to_csv(file, coins)

    # Transform my coins and the money to return into integer variables.
    coins, money_to_return, _ = to_integers(coins, money_to_return)

    # A dictionnary that make match a coin with its index where it should be stored.
    coins_to_index = coins_matching(coins)

    write_coins = write_coins_to_file(file)

    make_change_all_solutions_iterative_(coins, money_to_return, coins_to_index, write_coins)

def make_change_all_solutions_iterative_print(coins, money_to_return):
    """
        Execute the make change all solutions iteratively algorithm.
        Every solution are print in the console.
    """

    # Transform my coins and the money to return into integer variables.
    coins, money_to_return, _ = to_integers(coins, money_to_return)

    # A dictionnary that make match a coin with its index where it should be stored.
    coins_to_index = coins_matching(coins)

    make_change_all_solutions_iterative_(coins, money_to_return, coins_to_index, print_solution)
