from tests.test_make_change_greedy_solution import *
from tests.test_make_change_all_solution_recursive import *
from tests.test_make_change_all_solutions_iterative import *
from tests.test_make_change_best_solutions_recursive import *
from tests.test_make_change_solutions_with_cut import *
from tests.test_make_change_comparison import *
from tests.test_make_change_number_of_solutions import *
from tests.test_n_queens import *

# Test number of solutions:
test_make_change_number_of_solutions_print()
test_make_change_count_line_files()

# Tests greedy solution:
# test_greedy_solution_store_in_file()
# test_greedy_solution_print()

# Tests all solutions recursively:
# test_make_change_all_solutions_recursive_store_in_file()
# test_make_change_all_solutions_recursive_print()

# Tests all solutions iteratively:
# test_make_change_all_solutions_iterative_store_in_file()
# test_make_change_all_solutions_iterative_print()

# Tests all solutions with cut, price and share:
# test_make_change_solutions_recursive_with_cut_store_in_file()
# test_make_change_solutions_recursive_with_cut_print()

# Tests best solutions:
# test_make_change_best_solutions_recursive_store_in_file()
# test_make_change_best_solutions_recursive_print()

# Tests comparison times:
# test_make_change_store_in_file_comparison()
# test_make_change_print_comparison()
# test_make_change_print_and_store_in_file_comparison()

# Test n queens:
# test_n_queens_board_of_size_4()